package com.telerik.virtual_car_garage.Role;

import com.telerik.virtual_car_garage.entities.UserRoles;
import com.telerik.virtual_car_garage.repositories.RoleRepository;
import com.telerik.virtual_car_garage.services.RoleService;
import com.telerik.virtual_car_garage.services.RoleServiceImpl;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

public class RoleServiceImplTests {

    @Test
    public void createRole_Calls_RoleRepository_CreateRole() {
        UserRoles userRoles=new UserRoles();
        RoleRepository roleRepository = Mockito.mock(RoleRepository.class);
        RoleService service = new RoleServiceImpl(roleRepository);
        service.createRole(userRoles);

        Mockito.verify(roleRepository, Mockito.times(1)).createRole(userRoles);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createRole_Throws_Exception() throws IllegalArgumentException {
        RoleRepository roleRepository = Mockito.mock(RoleRepository.class);
        RoleService service = new RoleServiceImpl(roleRepository);
        service.createRole(null);
    }
}
