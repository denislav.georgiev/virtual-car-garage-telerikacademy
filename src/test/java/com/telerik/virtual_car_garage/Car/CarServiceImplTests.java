package com.telerik.virtual_car_garage.Car;

import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.repositories.CarRepository;
import com.telerik.virtual_car_garage.services.CarService;
import com.telerik.virtual_car_garage.services.CarServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class CarServiceImplTests {

    @Test
    public void getAllCars_Calls_CarRepository_getAllCars() {
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        service.getAllCars();
        Mockito.verify(carRepository, Mockito.times(1)).getAllCars();
    }

    @Test
    public void saveAll_Calls_CarRepository_saveAll() {
        List<Car> cars=new ArrayList<>();
        cars.add(createCars());
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        service.saveAll(cars);
        Mockito.verify(carRepository, Mockito.times(1)).saveAll(cars);
    }

    @Test
    public void findAll_Calls_CarRepository_findAll() {
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        service.findAll();
        Mockito.verify(carRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void createCar_Calls_CarRepository_createCar() {
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        Car car=createCars();
        service.createCar(car);
        Mockito.verify(carRepository, Mockito.times(1)).createCar(car);
    }

    @Test
    public void findById_Calls_CarRepository_findById() {
        long carId=1;
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        service.findById(carId);
        Mockito.verify(carRepository, Mockito.times(1)).findById(carId);
    }

    @Test
    public void deleteById_Calls_CarRepository_deleteById() {
        long carId=1;
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        service.deleteById(carId);
        Mockito.verify(carRepository, Mockito.times(1)).deleteById(carId);
    }

    @Test
    public void deleteByUserId_Calls_CarRepository_deleteByUserId() {
        long userId=1;
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        CarService service = new CarServiceImpl(carRepository);
        service.deleteByUserId(userId);
        Mockito.verify(carRepository, Mockito.times(1)).deleteByUserId(userId);
    }

    private Car createCars() {
        return new Car("test", "test", "2009", "test",
                "009281818");
    }


}
