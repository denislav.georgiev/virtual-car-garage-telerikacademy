package com.telerik.virtual_car_garage.entities;

public enum ServiceType {
    Fuel_Filter_Change("Fuel Filter Change"),Air_Filter_Change("Air Filter Change"),Engine_Oil_Change("Engine Oil Change"),Oil_Filter_Change("Oil Filter Change");

    private final String displayValue;

    private ServiceType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}

