package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.entities.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getUser(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User where username = :username", User.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    @Override
    public void createUser(User user) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.save(user);
        } catch (HibernateException e) {
            throw e;
        }
    }

    @Modifying
    @Transactional
    @Override
    public void changeUserPassword(User user, String password) {
        Session session = sessionFactory.getCurrentSession();
        String username = user.getUsername();
        Query query = session.createQuery("UPDATE User SET password = :password WHERE username = :username");
        query.setParameter("username", username);
        query.setParameter("password", passwordEncoder.encode(password));
        user.setPassword(passwordEncoder.encode(password));
        query.executeUpdate();

    }


    @Modifying
    @Transactional
    @Override
    public void changeUserUserName(User user, String newUsername) {
        Session session = sessionFactory.getCurrentSession();
        String username = user.getUsername();
        Query query = session.createQuery("UPDATE User SET username = :newUsername WHERE username = :username");
        query.setParameter("username", username);
        query.setParameter("newUsername", newUsername);
        user.setUsername(newUsername);
        query.executeUpdate();
    }

    @Modifying
    @Transactional
    @Override
    public void changeUserUserNameInUserRoles(User user, String newUsername) {
        Session session = sessionFactory.getCurrentSession();
        String username = user.getUsername();
        Query query = session.createQuery("UPDATE user_roles SET username = :newUsername WHERE username = :username");
        query.setParameter("username", username);
        query.setParameter("newUsername", newUsername);
        query.executeUpdate();
    }

    @Override
    public List<Car> getAllMyCars(long user_id) {
        Session session = sessionFactory.getCurrentSession();
        Query<Car> query = session.createQuery("from Car where user_id = :user_id", Car.class);
        query.setParameter("user_id", user_id);
        return query.getResultList();
    }

    @Override
    public User findByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User where username = :username", User.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    @Override
    public List<User> findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session
                .createQuery("from User", User.class)
                .list();
    }

    @Modifying
    @Transactional
    @Override
    public void deleteById(long user_id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from User where user_id = :user_id");
        query.setParameter("user_id", user_id);
        query.executeUpdate();
    }

    @Modifying
    @Transactional
    @Override
    public void deleteByIdFromUserRoles(long user_id) {
        Session session = sessionFactory.getCurrentSession();
        User user = findById(user_id);
        String username = user.getUsername();
        Query query = session.createQuery("delete from user_roles where username = :username");
        query.setParameter("username", username);
        query.executeUpdate();
    }

    @Override
    public User findById(long user_id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where user_id = :user_id", User.class);
        query.setParameter("user_id", user_id);
        return (User) query.getSingleResult();
    }


}


