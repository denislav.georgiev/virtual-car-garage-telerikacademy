package com.telerik.virtual_car_garage.entities;

public class EngineOilChange extends Services{

    @Override
    public void setDescription(String description) {
        super.setDescription("");
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(120);
    }

    @Override
    public String doChange() {
        return "Engine Filter Changed";
    }
}
