package com.telerik.virtual_car_garage.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_avatars")
public class UserAvatar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "avatar_id")
    private long id;

    @JoinColumn(name = "user_id")
    private long user_id;

    @Column(name = "avatar_name")
    @NotNull
    private String name;

    @Column(name = "type")
    @NotNull
    private String type;

    @Lob
    private byte[] image;

    public UserAvatar() {
    }

    public UserAvatar(long id, @NotNull String name) {
        this.name = name;
    }

    public UserAvatar(long user_id, @NotNull String name, @NotNull String type, byte[] image) {
        this.user_id =user_id;
        this.name=name;
        this.type=type;
        this.image=image;
    }

    public UserAvatar(int id, String name) {
        this.id=id;
        this.name=name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
