package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.entities.User;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.List;

public interface UserRepository {

    List<User> getUser(String username);

    void createUser(User user);

   // void createCriteriaUpdate(User user);

    void changeUserPassword(User user, String password);

    @Modifying
    @Transactional
    void changeUserUserName(User user, String newUsername);

    @Modifying
    @Transactional
    void changeUserUserNameInUserRoles(User user, String newUsername);

    List<Car> getAllMyCars(long user_id);

    User findByUsername(String email);

    List<User> findAll();

    @Modifying
    @Transactional
    void deleteById(long user_id);

    @Modifying
    @Transactional
    void deleteByIdFromUserRoles(long user_id);

    User findById(long user_id);
}
