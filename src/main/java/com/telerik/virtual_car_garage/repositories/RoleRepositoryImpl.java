package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.UserRoles;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class RoleRepositoryImpl implements RoleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createRole(UserRoles role) {
        try {
            Session session=sessionFactory.getCurrentSession();
            session.save(role);
        } catch (Exception e) {
            throw new HibernateException(e);
        }
    }
}
