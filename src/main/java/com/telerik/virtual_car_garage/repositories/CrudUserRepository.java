package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface CrudUserRepository extends CrudRepository<User, Long> {
}
