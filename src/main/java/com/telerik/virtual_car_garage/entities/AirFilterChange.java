package com.telerik.virtual_car_garage.entities;

public class AirFilterChange extends Services {

    @Override
    public void setDescription(String description) {
        super.setDescription("");
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(43);
    }

    @Override
    public String doChange() {
        return "Air Filter Changed";
    }
}
