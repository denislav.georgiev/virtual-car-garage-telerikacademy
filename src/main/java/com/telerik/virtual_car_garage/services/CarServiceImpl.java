package com.telerik.virtual_car_garage.services;

import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.repositories.CarRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;

    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }
    @Override
    public List<Car> getAllCars()
    {
        return carRepository.getAllCars();
    }

    @Override
    public void saveAll(List<Car> cars) {
        carRepository.saveAll(cars);
    }
    @Override
    public List<Car> findAll() {
        return carRepository.findAll();
    }

    @Override
    public void createCar(Car car) {
        carRepository.createCar(car);
    }

    @Override
    public Car findById(long carId) {
        return carRepository.findById(carId);
    }

    @Override
    public void deleteById(long carId) {
        carRepository.deleteById(carId);
    }

    @Override
    public void deleteByUserId(long user_id) {carRepository.deleteByUserId(user_id);}

}
