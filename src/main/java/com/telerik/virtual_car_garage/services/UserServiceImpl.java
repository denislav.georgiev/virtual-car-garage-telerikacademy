package com.telerik.virtual_car_garage.services;


import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.repositories.RoleRepository;
import com.telerik.virtual_car_garage.repositories.UserRepository;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.OneToMany;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository=userRepository;
        this.passwordEncoder=bCryptPasswordEncoder;
    }

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository=userRepository;
    }

    @Override
    public List<Car> getAllMyCars(long user_id){
       return userRepository.getAllMyCars(user_id);
    }
@Override
public List<User> findAll(){
   return userRepository.findAll();
}

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void createUser(User user) {
       userRepository.createUser(user);
    }

    @Override
    public void changeUserPassword(User user, String password) {
        userRepository.changeUserPassword(user,password);
    }
    @Override
    public void changeUserUserNameInUserRoles(User user, String newUsername) {
        userRepository.changeUserUserNameInUserRoles(user,newUsername);
    }

    @Override
    public void deleteById(long user_id) {
    userRepository.deleteById(user_id);
    }

    @Override
    public void deleteByIdFromUserRoles(long user_id) {
        userRepository.deleteByIdFromUserRoles(user_id);
    }

        @Override
    public User findById(long user_id) {
        return userRepository.findById(user_id);
    }

    @Override
    public List<User> getUser(String username) {
        return userRepository.getUser(username);
    }

    @Override
    public boolean checkIfValidOldPassword(User user, String oldPassword) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }

    @Override
    public void changeUserUserName(User user, String newUsername) {
        userRepository.changeUserUserName(user,newUsername);
    }
}

