function validateForm() {
    var year = document.forms["addCars"]["year"].value;
    var model = document.forms["addCars"]["model"].value;
    var registration = document.forms["addCars"]["registration"].value;
    var manufacturer = document.forms["addCars"]["manufacturer"].value;
    var VIN = document.forms["addCars"]["VIN"].value;

    if (year === "") {
        alert("Year must be filled out");
        return false;
    }
    if (model === "") {
        alert("Model must be filled out");
        return false;
    } if (registration === "") {
        alert("Registration must be filled out");
        return false;
    } if (manufacturer === "") {
        alert("Manufacturer must be filled out");
        return false;
    } if (VIN === "") {
        alert("VIN must be filled out");
        return false;
    }
}