package com.telerik.virtual_car_garage.services;

import com.telerik.virtual_car_garage.entities.UserAvatar;

public interface UserAvatarService {

    UserAvatar uploadAvatar(String username, UserAvatar userAvatar);

    UserAvatar getAvatar(long id);
}
