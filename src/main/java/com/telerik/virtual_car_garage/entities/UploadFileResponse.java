package com.telerik.virtual_car_garage.entities;

public class UploadFileResponse {
    private String avatarName;
    private String avatarUri;
    private String fileType;
    private long size;

    public UploadFileResponse() {
    }

    public UploadFileResponse(String avatarName, String avatarUri, String fileType, long size) {
        this.avatarName = avatarName;
        this.avatarUri = avatarUri;
        this.fileType = fileType;
        this.size = size;
    }

    public String getAvatarName() {
        return avatarName;
    }

    public void setAvatarName(String avatarName) {
        this.avatarName = avatarName;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
