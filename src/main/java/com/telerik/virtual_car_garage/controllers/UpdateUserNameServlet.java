package com.telerik.virtual_car_garage.controllers;

import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users/updateUsername")
public class UpdateUserNameServlet extends HttpServlet {

    @Autowired
    private UserService userService;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        String newUsername = request.getParameter("newusername");

        User user = userService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        userService.changeUserUserNameInUserRoles(user,newUsername);

        userService.changeUserUserName(user, newUsername);

        response.sendRedirect("successfulUsernameChange");

    }
}

