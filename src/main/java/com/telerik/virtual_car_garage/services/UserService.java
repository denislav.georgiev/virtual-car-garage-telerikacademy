package com.telerik.virtual_car_garage.services;

import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService {

    List<Car> getAllMyCars(long user_id);

    List<User> findAll();

    User findByUsername(String username);

    void createUser(User user);

    void changeUserPassword(User user, String password);

     boolean checkIfValidOldPassword(final User user, final String oldPassword) ;

    void changeUserUserName(User user, String newUsername);

    void changeUserUserNameInUserRoles(User user, String newUsername);

    void deleteById(long user_id);

    void deleteByIdFromUserRoles(long user_id);

    User findById(long user_id);

    List<User> getUser(String username);
}
