package com.telerik.virtual_car_garage.services;

import com.telerik.virtual_car_garage.entities.UserAvatar;
import com.telerik.virtual_car_garage.repositories.UserAvatarRepository;
import com.telerik.virtual_car_garage.repositories.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class UserAvatarServiceImpl implements UserAvatarService {
    private UserRepository userRepository;
    private UserAvatarRepository userAvatarRepository;

    public UserAvatarServiceImpl(UserRepository userRepository, UserAvatarRepository userAvatarRepository) {
        this.userRepository = userRepository;
        this.userAvatarRepository = userAvatarRepository;
    }

    @Override
    public UserAvatar uploadAvatar(String username, UserAvatar userAvatar) {
        if (userRepository.findByUsername(username) == null) {
            throw new IllegalArgumentException(String.format("User with username %s not found.", username));
        }

        userAvatarRepository.save(userAvatar);
        return userAvatar;
    }

    @Override
    public UserAvatar getAvatar(long id) {
        return userAvatarRepository.findById(id)
                                   .orElseThrow(() -> new IllegalArgumentException("File not found with image id " + id));
    }
}
