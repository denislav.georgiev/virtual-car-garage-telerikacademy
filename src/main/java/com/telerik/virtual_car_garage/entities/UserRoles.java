package com.telerik.virtual_car_garage.entities;

import javax.persistence.*;

@Entity(name="user_roles")
public class UserRoles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_role_id")
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "role")
    private String role;

    public UserRoles() {
    }

    public UserRoles(String username, String role) {
        this.username=username;
        this.role=role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
