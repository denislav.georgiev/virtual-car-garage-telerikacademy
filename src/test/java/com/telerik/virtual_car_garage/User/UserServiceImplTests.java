package com.telerik.virtual_car_garage.User;

import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.repositories.UserRepository;
import com.telerik.virtual_car_garage.services.UserService;
import com.telerik.virtual_car_garage.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserService userService;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Test
    public void getAllMyCars_Calls_UserRepository_getAllMyCars() {
        long userId = 1;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.getAllMyCars(userId);

        Mockito.verify(userRepository, Mockito.times(1)).getAllMyCars(userId);
    }


    @Test
    public void findAll_Should_Return_Exact_Number() {
        UserRepository mockRepository = Mockito.mock(UserRepository.class);

        List<User> users = new ArrayList<>();

        User nestor = new User();
        nestor.setUsername("test1");

        User oliuv = new User();
        oliuv.setUsername("test2");

        User trax = new User();
        trax.setUsername("test3");


        users.add(nestor);
        users.add(oliuv);
        users.add(trax);


        Mockito.when(mockRepository.findAll()).thenReturn(users);

        //Act
        UserService service = new UserServiceImpl(mockRepository);

        List<User> result = service.findAll();

        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void findAll_Calls_UserRepository_findAll() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.findAll();
        Mockito.verify(userRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getUser_Calls_UserRepository_getUser() {
        // Arrange
        String username = "test";

        UserService service = new UserServiceImpl(userRepository);

        service.getUser(username);
        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).getUser(username);
    }


    @Test
    public void createUser_Should_Call_BCrypt_Encode() {
        //Act
        User user = createUser();
        BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);

        bCryptPasswordEncoder.encode(user.getPassword());
        //Assert
        Mockito.verify(bCryptPasswordEncoder, Mockito.times(1)).encode(user.getPassword());


    }

    @Test
    public void createUser_Should_Call_Repository_createUser() {
        //Act
        User user = createUser();

        UserService service = new UserServiceImpl(userRepository, bCryptPasswordEncoder);

        service.createUser(user);
        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).createUser(user);

    }


    @Test
    public void findByUserName_Calls_UserRepository_findByUserName() {

        String username = "test";

        UserService service = new UserServiceImpl(userRepository);

        service.findByUsername(username);

        Mockito.verify(userRepository, Mockito.times(1)).findByUsername(username);
    }

    @Test
    public void changeUserUserName_Calls_UserRepository_changeUserUserName() {
        User user = createUser();
        String username = "new";
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.changeUserUserName(user, username);
        Mockito.verify(userRepository, Mockito.times(1)).changeUserUserName(user, username);
    }

    @Test
    public void changeUserPassword_Calls_UserRepository_changeUserPassword() {
        User user = createUser();
        String password = "new";
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.changeUserPassword(user, password);
        Mockito.verify(userRepository, Mockito.times(1)).changeUserPassword(user, password);
    }

    @Test
    public void changeUserNameInUserRoles_Calls_UserRepository_changeUserNameInUserRoles() {
        User user = createUser();
        String newUsername = "new";
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.changeUserUserNameInUserRoles(user, newUsername);
        Mockito.verify(userRepository, Mockito.times(1)).changeUserUserNameInUserRoles(user, newUsername);
    }

    @Test
    public void deleteById_Calls_UserRepository_deleteById() {
        long userId = 1;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.deleteById(userId);
        Mockito.verify(userRepository, Mockito.times(1)).deleteById(userId);
    }

    @Test
    public void deleteByIdFromUserRoles_Calls_UserRepository_deleteByIdFromUserRoles() {
        long userId = 1;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.deleteByIdFromUserRoles(userId);
        Mockito.verify(userRepository, Mockito.times(1)).deleteByIdFromUserRoles(userId);
    }


    @Test
    public void findById_Calls_UserRepository_findById() {
        long userId = 1;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserService service = new UserServiceImpl(userRepository);
        service.findById(userId);
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
    }

    public User createUser() {
        return new User("test", "test", "test@gmail.com", "test",
                "009281818", true, true);
    }

}
