package com.telerik.virtual_car_garage.UserAvatar;

import com.telerik.virtual_car_garage.entities.UserAvatar;
import com.telerik.virtual_car_garage.repositories.UserAvatarRepository;
import com.telerik.virtual_car_garage.repositories.UserRepository;
import com.telerik.virtual_car_garage.services.UserAvatarService;
import com.telerik.virtual_car_garage.services.UserAvatarServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;


public class UserAvatarImplTests {


    @Test(expected = IllegalArgumentException.class)
    public void uploadAvatar_Throws_Exception_If_UserNotFound() {
        long id = 1;
        UserAvatar userAvatar = new UserAvatar(1, "test");
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserAvatarRepository userAvatarRepository = Mockito.mock(UserAvatarRepository.class);
        UserAvatarService userAvatarService = new UserAvatarServiceImpl(userRepository, userAvatarRepository);
        userAvatarService.uploadAvatar(String.valueOf(id), userAvatar);
        Mockito.verify(userAvatarRepository, Mockito.times(1)).getOne(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAvatar_Throws_Exception_If_ImageId_NotFound() {
        long id = 1;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UserAvatarRepository userAvatarRepository = Mockito.mock(UserAvatarRepository.class);
        UserAvatarService userAvatarService = new UserAvatarServiceImpl(userRepository, userAvatarRepository);
        userAvatarService.getAvatar(id);
        Mockito.verify(userAvatarRepository, Mockito.times(1)).findById(id);
    }
}

