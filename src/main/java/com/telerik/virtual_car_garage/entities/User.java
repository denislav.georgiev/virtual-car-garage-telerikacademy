package com.telerik.virtual_car_garage.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    //@NotBlank(message = "Name is mandatory")
    //@Min(value = 4,message = "Name must be minimum {min} characters in length.")
    @Column(name = "name")
    private String name;

    //@Min(value = 4,message = "Username must be minimum {min} characters in length.")
    @Column(name = "username")
    private String username;

   //@Email
   //@NotBlank(message = "Email is mandatory")
    @Column(name = "email_id")
    private String email;

   // @Min(value = 6, message = "Password must be minimum {min} characters in length.")
    @Column(name = "password")
    private String password;

   // @Size(min=10,max = 10, message = "Phone must be {min} to {max} characters in length.")
    @Column(name = "phone")
    private String phone;

    private boolean isEnabled;

    private boolean tokenExpired;

    @OneToMany
    @JoinTable(name = "users_cars")
    private List<Car> myCars = new ArrayList<>();

    public User() {
    }


    public User(String email) {
        this.email = email;
    }

    public User(String name, String username, String email, String password, String phone, boolean isEnabled, boolean tokenExpired) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.isEnabled = isEnabled;
        this.tokenExpired = tokenExpired;
    }


    public User(String email, String name, String phone) {
        this.phone = phone;
        this.email = email;
        this.name = name;
    }

    public List<Car> getMyCars() {
        return myCars;
    }

    public void setMyCars(List<Car> myCars) {
        this.myCars =myCars;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }


    public String getPassword() {
        return password;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String emailId) {
        this.email = emailId;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
