package com.telerik.virtual_car_garage.controllers;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.entities.Contact;
import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.repositories.CrudCarRepository;
import com.telerik.virtual_car_garage.repositories.CrudUserRepository;
import com.telerik.virtual_car_garage.services.CarService;
import com.telerik.virtual_car_garage.services.UserService;
import javassist.tools.web.BadHttpRequest;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.support.RequestHandledEvent;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private CrudUserRepository crudUserRepository;

    @Autowired
    private CarService carService;

    @Autowired
    private CrudCarRepository crudCarRepository;

    @GetMapping("/admin/allCars")
    public String showAllCars(Model model) {
        try{
        List<Car> cars=carService.getAllCars();
        model.addAttribute("cars",cars);
        }catch(SpelEvaluationException ex){
            return  null;
        }
        return "admin/cars";
    }

    @GetMapping("/admin/allUsers")
    public String showAllUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "admin/users";
    }

    @GetMapping("/admin/InsertingData")
    public String insertingData(@ModelAttribute Contact contact) {
        return "admin/successUser";
    }


    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("admin/admin_home");
        return model;

    }

    @RequestMapping(value = "/admin/createUserPage", method = RequestMethod.GET)
    public ModelAndView adminCreateUser() {
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/createUserPage");
        return model;

    }

    @RequestMapping(value = "/admin/editUser/{id}")
    public String editUser(@PathVariable("id") Long userId, Model model) {
        model.addAttribute("user", userService.findById(userId));
        return "admin/editUser";
    }

    @RequestMapping(value = "/admin/saveUser", method = RequestMethod.POST)
    public String saveUser(User user) throws Exception {
        try {
            User user1 = userService.findById(user.getId());
            user.setPassword(user1.getPassword());
            user.setEnabled(true);
            user.setTokenExpired(true);
            user.setUsername(user1.getUsername());
            crudUserRepository.save(user);
        } catch (HibernateException ex) {
           throw new Exception(ex);
        }
        return "redirect:allUsers";
    }

    @RequestMapping(value = "/admin/deleteUser/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("id") long user_id) throws Exception {
        try {
            carService.deleteByUserId(user_id);
            userService.deleteByIdFromUserRoles(user_id);
            userService.deleteById(user_id);
        } catch (NoResultException ex) {
            throw new Exception("No entity found but delete passed.");
        }
        return "redirect:/admin/allUsers";
    }

    @RequestMapping(value = "/admin/addCars")
    public String createCar(Model model) {
        model.addAttribute("car", new Car());
        return "admin/addCars";
    }

    @RequestMapping(value = "/admin/saveCar", method = RequestMethod.POST)
    public String saveCar(Car car) throws Exception {
        try {
            crudCarRepository.save(car);
        } catch (SpelEvaluationException ex) {
            throw new Exception(ex);
        }
        return "redirect:allCars";
    }

    @RequestMapping(value = "/admin/editCar/{id}")
    public String editCar(@PathVariable("id") long carId, Model model) {
        Car car = carService.findById(carId);
        model.addAttribute("car", car);
        return "admin/editCars";
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.GET)
    public String deleteCar(@PathVariable("id") long carId) {
        try {
            carService.deleteById(carId);
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        return "redirect:allCars";
    }

}
