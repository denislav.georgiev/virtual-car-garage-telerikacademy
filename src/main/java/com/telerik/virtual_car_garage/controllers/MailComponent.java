package com.telerik.virtual_car_garage.controllers;

import com.telerik.virtual_car_garage.entities.Contact;

import com.telerik.virtual_car_garage.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;

import java.nio.charset.StandardCharsets;
import java.util.Random;


@Component
public class MailComponent {

    @Autowired
    private MailSender mailSender;

    boolean sendSimpleMail(Contact contact) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(contact.getEmail());
        mailMessage.setSubject(contact.getSubject());
        mailMessage.setText("Email:" + contact.getEmail() + "\n" + "Name:" + contact.getName() + "\n" + "Phone:" + contact.getPhone());
        mailMessage.setTo("denislavboyanovgeorgiev@gmail.com");

        try {
            mailSender.send(mailMessage);
            return true;
        } catch (MailException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    void sendConfirmationMail(User user, String password) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("denislavboyanovgeorgiev@gmail.com");
        mailMessage.setSubject("Account Created !");
        mailMessage.setText("Your password is :" + user.getUsername() + ", Your username is :" + password);
        mailMessage.setTo(user.getEmail());
        try {
            mailSender.send(mailMessage);
        } catch (MailException e) {
            System.err.println(e.getMessage());
        }

    }
}