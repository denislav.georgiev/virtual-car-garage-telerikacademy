package com.telerik.virtual_car_garage.services;

import com.telerik.virtual_car_garage.entities.Car;

import java.util.List;

public interface CarService {

    List<Car> getAllCars();

    void saveAll(List<Car> cars);

    List<Car> findAll();

    void createCar(Car car);

    Car findById(long carId);

    void deleteById(long carId);

    void deleteByUserId(long user_id);
}
