package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.Car;
import org.springframework.data.repository.CrudRepository;

public interface CrudCarRepository extends CrudRepository<Car, Long> {
}
