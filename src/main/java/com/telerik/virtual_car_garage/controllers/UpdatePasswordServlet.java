package com.telerik.virtual_car_garage.controllers;

import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users/updatePass")
public class UpdatePasswordServlet extends HttpServlet {

    @Autowired
    UserService userService;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        String oldPassword = request.getParameter("oldpassword");
        String newPass = request.getParameter("password");

        User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        if (!userService.checkIfValidOldPassword(user, oldPassword)) {
            throw new IllegalArgumentException();
        }

        userService.changeUserPassword(user,newPass);

        response.sendRedirect("successfulPasswordChange");


    }
}
