package com.telerik.virtual_car_garage.entities;

import org.hibernate.validator.constraints.Length;
import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class Contact {

//    @NotBlank(message = "Name is mandatory")
//    @Min(value = 4,message = "Name must be minimum {min} characters in length.")
    @Column(name = "name")
    private String name;

//    @Email
//    @NotBlank(message = "Email is mandatory")
    @Column(name = "email_id")
    private String email;

//    @Length(min=10,max = 10, message = "Phone must be {min} to {max} characters in length.")
//    @Pattern(regexp="^(0|[1-9][0-9]*)$")
//    @NotBlank(message = "Phone is mandatory")
    @Column(name = "phone")
    private String phone;

    private String subject;

    private String message;

    public Contact() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}