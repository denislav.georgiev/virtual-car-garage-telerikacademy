package com.telerik.virtual_car_garage.controllers;

import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.entities.UserRoles;
import com.telerik.virtual_car_garage.services.RoleService;
import com.telerik.virtual_car_garage.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.*;
import java.lang.*;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet("/admin/InsertingData")
public class InsertingDataServlet extends HttpServlet {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private MailComponent mailComponent;

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String role = request.getParameter("role");


            out.println(name);
            out.println(email);
            out.println(phone);
            out.println(role);

            String username=stringGenerator(5);
            String password =stringGenerator(7);

            UserRoles userRoles = new UserRoles(username, role);
            User user = new User(name, username, email, bCryptPasswordEncoder.encode(password), phone, true, true);

            userService.createUser(user);
            roleService.createRole(userRoles);

            mailComponent.sendConfirmationMail(user,password);

            out.println("<br>Record has been inserted");

        } catch (Exception e) {
            out.println(e);
        }
    }

    private static String stringGenerator(int n)
    {
        // length is bounded by 7 Character
        byte[] array = new byte[256];
        new Random().nextBytes(array);

        String randomString
                = new String(array, StandardCharsets.UTF_8);

        StringBuffer r = new StringBuffer();

        String  AlphaNumericString
                = randomString
                .replaceAll("[^A-Za-z0-9]", "");

        for (int k = 0; k < AlphaNumericString.length(); k++) {

            if (Character.isLetter(AlphaNumericString.charAt(k))
                    && (n > 0)
                    || Character.isDigit(AlphaNumericString.charAt(k))
                    && (n > 0)) {

                r.append(AlphaNumericString.charAt(k));
                n--;
            }
        }
        return r.toString();
    }
}

