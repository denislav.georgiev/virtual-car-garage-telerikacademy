package com.telerik.virtual_car_garage.entities;

import javax.persistence.Entity;

public abstract class Services {

    private String description;

    private double price;

    public Services() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public abstract String doChange();

}
