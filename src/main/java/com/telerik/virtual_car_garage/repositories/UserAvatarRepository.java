package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.UserAvatar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAvatarRepository extends JpaRepository<UserAvatar, Long> {
}
