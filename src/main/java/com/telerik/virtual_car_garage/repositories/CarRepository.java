package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.Car;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.List;

public interface CarRepository {
    List<Car> getAllCars();

    void saveAll(List<Car> cars);

    void createCar(Car car);

    Car findById(long carId);

    @Modifying
    @Transactional
    void deleteById(long carId);

    @Modifying
    @Transactional
    void deleteByUserId(long user_id);

    List<Car> findAll();
}
