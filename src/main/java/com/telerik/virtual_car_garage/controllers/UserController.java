package com.telerik.virtual_car_garage.controllers;

import com.telerik.virtual_car_garage.entities.Car;
import com.telerik.virtual_car_garage.entities.Contact;
import com.telerik.virtual_car_garage.entities.User;
import com.telerik.virtual_car_garage.repositories.CrudCarRepository;
import com.telerik.virtual_car_garage.services.CarService;
import com.telerik.virtual_car_garage.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.spring5.processor.SpringInputGeneralFieldTagProcessor;

import javax.persistence.NoResultException;
import javax.validation.Valid;


@Controller
public class UserController {

    @Autowired
    private CrudCarRepository crudCarRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CarService carService;

    @RequestMapping("/users/profile")
    public String userProfile(Model model,Authentication authentication) {
        String username=authentication.getName();
        model.addAttribute("users",userService.getUser(username));
        return "users/profile";
    }

    @RequestMapping(value = "/users/home", method = RequestMethod.GET)
    public ModelAndView userPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("users/home");
        return model;

    }

    @GetMapping(value = "/users/updatePassword")
    public ModelAndView userUpdatePassword() {
        ModelAndView model = new ModelAndView();
        model.setViewName("users/updatePassword");
        return model;

    }
    @GetMapping(value = "/users/updateUserName")
    public ModelAndView userUpdateUserName() {
        ModelAndView model = new ModelAndView();
        model.setViewName("users/updateUserName");
        return model;

    }
    @GetMapping(value = "/users/successfulPasswordChange")
    public ModelAndView userPasswordChangeSuccess() {
        ModelAndView model = new ModelAndView();
        model.setViewName("users/successfulPasswordChange");
        return model;

    }

    @GetMapping(value = "/users/successfulUsernameChange")
    public ModelAndView userUsernameChangeSuccess() {
        ModelAndView model = new ModelAndView();
        model.setViewName("users/successfulUsernameChange");
        return model;

    }
    @GetMapping(value = "/successfulRegistration")
    public ModelAndView successfulRegistration() {
        ModelAndView model = new ModelAndView();
        model.setViewName("successfulRegistration");
        return model;

    }

    @RequestMapping(value = "/users/addCars")
    public String createCar(Model model){
        model.addAttribute("car",new Car());
        return "users/addCars";
    }

    @RequestMapping(value = "/users/saveCar", method = RequestMethod.POST)
    public String saveCar(@Valid Car car, Authentication authentication){
        String username=authentication.getName();
        User user=userService.findByUsername(username);
        long userId=user.getId();
        car.setUser(user);
        userService.getAllMyCars(userId).add(car);
        crudCarRepository.save(car);
        try{
            return "redirect:myCars";}catch (NumberFormatException ex){
           throw new IllegalArgumentException();
        }
    }

    @RequestMapping(value = "/users/editCar/{id}")
    public String editCar(@PathVariable("id") long carId, Model model){
        try {
            Car car = carService.findById(carId);
            model.addAttribute("car",car);
        }catch (TemplateProcessingException ex){
            return null;
        }
        return "users/editCars";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String deleteCar(@PathVariable long id) {
        try{
            carService.deleteById(id);
        }catch (NoResultException ex){
            return null;
        }
        return "redirect:myCars";
    }


    @GetMapping("users/myCars")
    public String myCars(Authentication authentication, Model model) {
        String username=authentication.getName();
        User user=userService.findByUsername(username);
        long userId=user.getId();
        model.addAttribute("cars", userService.getAllMyCars(userId));
        return "users/cars";
    }

}


