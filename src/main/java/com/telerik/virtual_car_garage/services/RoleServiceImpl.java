package com.telerik.virtual_car_garage.services;

import com.telerik.virtual_car_garage.entities.UserRoles;
import com.telerik.virtual_car_garage.repositories.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public void createRole(UserRoles role) {
        if(role==null){
            throw new IllegalArgumentException("Role cannot be null or empty !");
        }
        roleRepository.createRole(role);
    }
}

