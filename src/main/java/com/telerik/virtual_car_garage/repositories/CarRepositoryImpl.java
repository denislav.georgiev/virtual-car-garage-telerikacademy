package com.telerik.virtual_car_garage.repositories;

import com.telerik.virtual_car_garage.entities.Car;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
@Transactional
public class CarRepositoryImpl implements CarRepository {

    private SessionFactory sessionFactory;

    private static Map<Long, Car> carMap = new HashMap<>();

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Car> getAllCars() {
        try {
            Session session = sessionFactory.getCurrentSession();
            return session
                    .createQuery("from Car", Car.class)
                    .list();
        } catch (Exception ex) {
            throw new HibernateException(ex);
        }
    }

    @Override
    public void saveAll(List<Car> cars) {
        long nextId = getNextId();
        for (Car car : cars) {
            if (car.getId() == 0) {
                car.setId(nextId++);
            }
        }

        Map<Long, Car> carMap = cars.stream()
                .collect(Collectors.toMap(Car::getId, Function.identity()));

        CarRepositoryImpl.carMap.putAll(carMap);
    }

    @Override
    public void createCar(Car car) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.save(car);
        } catch (Exception e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public Car findById(long carId) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<Car> query = session.createQuery("from Car where car_id = :car_id", Car.class);
            query.setParameter("car_id", carId);
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }


    }


    @Modifying
    @Transactional
    @Override
    public void deleteById(long carId) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery("delete from Car where car_id = :carId");
            query.setParameter("carId", carId);
            query.executeUpdate();
        } catch (Exception ex) {
            throw new HibernateException(ex);
        }
    }

    @Modifying
    @Transactional
    @Override
    public void deleteByUserId(long user_id) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery("delete from Car where user_id = :user_id");
            query.setParameter("user_id", user_id);
            query.executeUpdate();
        } catch (Exception ex) {
            throw new NoSuchElementException();
        }
    }

    @Override
    public List<Car> findAll() {
        return new ArrayList<>(carMap.values());
    }

    private Long getNextId() {
        return carMap.keySet()
                .stream()
                .mapToLong(value -> value)
                .max()
                .orElse(0) + 1;
    }
}

