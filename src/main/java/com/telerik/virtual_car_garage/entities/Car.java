package com.telerik.virtual_car_garage.entities;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id")
    private long id;

    @NotNull
    @Column(name = "manufacturer")
    private String manufacturer;

    @NotNull
    @Column(name = "model")
    private String model;

    @NotNull
    //@Size(min = 4, max = 4, message = "Year must be {min} to {max} characters in length.")
    @Column(name = "year")
    private String year;

   // @Size(min = 8, max = 8, message = "Registration must be {min} to {max} characters in length.")
    @Column(name = "registration")
    private String registration;

   // @Size(min = 11, max = 17, message = "VIN must be {min} to {max} characters in length.")
    @Column(name = "VIN")
    private String VIN;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "service_type")
    private ServiceType serviceType;

    public Car() {
    }

    public Car(String manufacturer, String model, String year, String registration, String VIN,User user,ServiceType serviceType) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
        this.registration = registration;
        this.VIN = VIN;
        this.user=user;
        this.serviceType=serviceType;
    }

    public Car(String registration, String manufacturer, String model, String year, String VIN) {
        this.registration=registration;
        this.manufacturer=manufacturer;
        this.model=model;
        this.year=year;
        this.VIN=VIN;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }
}
